//
//  Movie.h
//  vrooom
//
//  Created by Konstantin Anoshkin on 25.12.15.
//  Copyright 2015 Konstantin Anoshkin. All rights reserved.
//

@import Foundation;


@interface Movie : NSObject

@property (nonatomic) NSUInteger identifier;
@property (nonatomic, strong) NSURL * URL;
@property (nonatomic) NSTimeInterval duration;
@property (nonatomic) NSTimeInterval threadDuration;
@property (nonatomic) NSTimeInterval offset;
@property (nonatomic) NSUInteger parentIdentifier;

@end
