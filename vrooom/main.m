//
//  main.m
//  vrooom
//
//  Created by Konstantin Anoshkin on 25.12.15.
//  Copyright © 2015 Konstantin Anoshkin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
