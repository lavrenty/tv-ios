//
//  CaptureViewController.h
//  vrooom
//
//  Created by Konstantin Anoshkin on 25.12.15.
//  Copyright 2015 Konstantin Anoshkin. All rights reserved.
//

@import UIKit;


@class Movie, AFHTTPSessionManager;

@interface CaptureViewController : UIViewController

@property (nonatomic, strong) AFHTTPSessionManager * sessionManager;
@property (nonatomic, strong) Movie * movieRepliedTo;

@end
