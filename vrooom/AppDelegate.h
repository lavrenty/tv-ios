//
//  AppDelegate.h
//  vrooom
//
//  Created by Konstantin Anoshkin on 25.12.15.
//  Copyright © 2015 Konstantin Anoshkin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

