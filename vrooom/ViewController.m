//
//  ViewController.m
//  vrooom
//
//  Created by Konstantin Anoshkin on 25.12.15.
//  Copyright © 2015 Konstantin Anoshkin. All rights reserved.
//

#import "ViewController.h"
#import "CaptureViewController.h"
#import "Movie.h"
#import "AFHTTPSessionManager.h"
@import AVFoundation;


@interface ViewController ()

@property (nonatomic, strong) AFHTTPSessionManager * sessionManager;
@property (nonatomic, strong) NSArray <Movie *> * movies;
@property (nonatomic) NSUInteger currentMovieIndex;
@property (nonatomic, strong) AVQueuePlayer * player;
@property (nonatomic, strong) AVPlayerLayer * playerLayer;
@property (nonatomic, weak) IBOutlet UIButton * replyButton;
@property (nonatomic, weak) IBOutlet UIProgressView * fileProgressView;
@property (nonatomic, weak) IBOutlet UIProgressView * threadProgressView;

@end


#pragma mark -


@implementation ViewController


- (void) viewDidLoad
{
	[super viewDidLoad];
	
	NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration ephemeralSessionConfiguration];
	configuration.requestCachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
	configuration.timeoutIntervalForRequest = 30.0;
	configuration.timeoutIntervalForResource = 300.0;
	configuration.networkServiceType = NSURLNetworkServiceTypeDefault;
	configuration.allowsCellularAccess = YES;
	configuration.connectionProxyDictionary = CFBridgingRelease(CFNetworkCopySystemProxySettings());
	configuration.TLSMinimumSupportedProtocol = kTLSProtocol12;
	configuration.HTTPShouldUsePipelining = YES;
	configuration.HTTPShouldSetCookies = NO;
	configuration.HTTPCookieAcceptPolicy = NSHTTPCookieAcceptPolicyNever;
	configuration.HTTPAdditionalHeaders = @{ @"Accept": @"application/json" };
	configuration.HTTPMaximumConnectionsPerHost = 1;
	configuration.HTTPCookieStorage = nil;
	configuration.URLCache = nil;
	
	AFJSONResponseSerializer *serializer = [AFJSONResponseSerializer new];
	serializer.removesKeysWithNullValues = YES;
	
	self.sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL: [NSURL URLWithString: @"http://tv.notconspiracy.com/"] sessionConfiguration: configuration];
	self.sessionManager.responseSerializer = serializer;
	
	__weak typeof(self) weakSelf = self;
	self.player = [AVQueuePlayer queuePlayerWithItems: @[]];
	[self.player addObserver: self forKeyPath: @"currentItem" options: 0 context: @"currentItem"];
	[self.player addPeriodicTimeObserverForInterval: CMTimeMakeWithSeconds(0.5, 30) queue: nil usingBlock: ^(CMTime time) {
		if (weakSelf.currentMovieIndex < weakSelf.movies.count) {
			Movie *currentMovie = weakSelf.movies[weakSelf.currentMovieIndex];
			weakSelf.fileProgressView.progress = CMTimeGetSeconds(time) / currentMovie.duration;
			weakSelf.threadProgressView.progress = (currentMovie.offset + CMTimeGetSeconds(time)) / currentMovie.threadDuration;
		}
	}];
	
	self.playerLayer = [AVPlayerLayer playerLayerWithPlayer: self.player];
	self.playerLayer.frame = self.view.bounds;
	[self.view.layer insertSublayer: self.playerLayer atIndex: 0];
	[self reload: nil];
}


- (IBAction) reload: (id) sender
{
	[self.sessionManager GET: @"/feed"
				  parameters: nil
					progress: nil
					 success: ^(NSURLSessionDataTask *task, NSDictionary *json) {
						 NSLog(@"%@", json);
						 NSMutableArray *movies = [NSMutableArray array];
						 [json[@"feed"] enumerateObjectsUsingBlock: ^(NSDictionary *movieInfo, NSUInteger idx, BOOL *stop) {
							 Movie *movie = [Movie new];
							 movie.identifier = [movieInfo[@"id"] unsignedIntegerValue];
							 movie.URL = [NSURL URLWithString: movieInfo[@"url"]];
							 movie.duration = [movieInfo[@"duration"] doubleValue];
							 movie.parentIdentifier = [movieInfo[@"parent"] unsignedIntegerValue];
							 [movies addObject: movie];
						 }];
						 self.movies = movies;
						 [self updateThreadDurations];
						 if (self.movies.firstObject) {
							 [self playMovie: self.movies.firstObject];
							 [self.player play];
						 }
					 }
					 failure: ^(NSURLSessionDataTask *task, NSError *error) {
						 [self reportError: error];
					 }];
}


- (void) updateThreadDurations
{
	for (Movie *threadMovie in self.movies) {
		if (threadMovie.parentIdentifier == 0) {
			threadMovie.offset = 0;
			NSMutableArray *thread = [NSMutableArray arrayWithObject: threadMovie];
			NSTimeInterval threadDuration = threadMovie.duration;
			for (Movie *movie in self.movies) {
				if (movie.parentIdentifier == threadMovie.identifier) {
					[thread addObject: movie];
					movie.offset = threadDuration;
					threadDuration += movie.duration;
				}
			}
			[thread setValue: @(threadDuration) forKey: @"threadDuration"];
		}
	}
}


- (void) observeValueForKeyPath: (NSString *) keyPath ofObject: (id) object change: (NSDictionary<NSString *,id> *) change context: (void *) context
{
	if (context == @"currentItem") {
		if (self.player.currentItem == nil) {
			[self skip: nil];
		}
	}
}


- (IBAction) like: (id) sender
{
	if (self.currentMovieIndex >= self.movies.count)
		return;
	[self.sessionManager POST: [NSString stringWithFormat: @"/like/%lu", (unsigned long) self.movies[self.currentMovieIndex].identifier]
				   parameters: nil
					 progress: nil
					  success: ^(NSURLSessionDataTask *task, id responseObject) {
						  
					  }
					  failure: ^(NSURLSessionDataTask *task, NSError *error) {
						  [self reportError: error];
					  }];
}


- (IBAction) dislike: (id) sender
{
	if (self.currentMovieIndex >= self.movies.count)
		return;
	[self.sessionManager POST: [NSString stringWithFormat: @"/unlike/%lu", (unsigned long) self.movies[self.currentMovieIndex].identifier]
				   parameters: nil
					 progress: nil
					  success: ^(NSURLSessionDataTask *task, id responseObject) {
						  
					  }
					  failure: ^(NSURLSessionDataTask *task, NSError *error) {
						  [self reportError: error];
					  }];
}


- (IBAction) skip: (id) sender
{
	if (self.movies.count == 0)
		return;
	
	NSUInteger nextMovieIndex = self.currentMovieIndex + 1;
	if (nextMovieIndex >= self.movies.count)
		nextMovieIndex = 0;
	
	self.currentMovieIndex = nextMovieIndex;
	[self playMovie: self.movies[nextMovieIndex]];
}


- (IBAction) skipToParent: (id) sender
{
	NSUInteger nextMovieIndex = self.currentMovieIndex + 1;
	if (nextMovieIndex >= self.movies.count)
		nextMovieIndex = 0;
	
	self.currentMovieIndex = NSNotFound;
	[self.movies enumerateObjectsAtIndexes: [NSIndexSet indexSetWithIndexesInRange: (NSRange) { nextMovieIndex, self.movies.count - nextMovieIndex }] options: 0 usingBlock: ^(Movie *movie, NSUInteger idx, BOOL *stop) {
		if (movie.parentIdentifier == 0) {
			self.currentMovieIndex = idx;
			[self playMovie: movie];
			*stop = YES;
		}
	}];
	
	if (self.currentMovieIndex == NSNotFound) {
		[self skip: nil];
	}
}


- (void) playMovie: (Movie *) movie
{
	[self.player replaceCurrentItemWithPlayerItem: [AVPlayerItem playerItemWithURL: movie.URL]];
	self.fileProgressView.progress = 0;
}


- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
	CaptureViewController *vc = (CaptureViewController *) segue.destinationViewController;
	vc.sessionManager = self.sessionManager;
	if (sender == self.replyButton) {
		vc.movieRepliedTo = self.currentMovieIndex < self.movies.count ? self.movies[self.currentMovieIndex] : nil;
	}
}


- (void) viewDidLayoutSubviews
{
	self.playerLayer.frame = self.view.bounds;
}


- (BOOL) prefersStatusBarHidden
{
	return YES;
}


- (void) reportError: (NSError *) error
{
	if (!error)
		return;
	
	UIAlertController *alert = [UIAlertController alertControllerWithTitle: error.localizedDescription message: error.localizedFailureReason preferredStyle: UIAlertControllerStyleAlert];
	[alert addAction: [UIAlertAction actionWithTitle: @"OK" style: UIAlertActionStyleDefault handler: nil]];
	[self presentViewController: alert animated: YES completion: nil];
}


@end
