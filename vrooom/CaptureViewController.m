//
//  CaptureViewController.m
//  vrooom
//
//  Created by Konstantin Anoshkin on 25.12.15.
//  Copyright 2015 Konstantin Anoshkin. All rights reserved.
//

#import "CaptureViewController.h"
#import "Movie.h"
#import "AFHTTPSessionManager.h"
@import AVFoundation;
#import "SDAVAssetExportSession.h"


@interface CaptureViewController () <AVCaptureFileOutputRecordingDelegate>

@property (nonatomic, strong) AVCaptureVideoPreviewLayer * previewLayer;
@property (nonatomic, strong) AVCaptureMovieFileOutput * fileOutput;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView * activityIndicator;
@property (nonatomic, weak) IBOutlet UIProgressView * progressView;
@property (nonatomic, weak) IBOutlet UIButton * recordButton;

@end


#pragma mark -


@implementation CaptureViewController


- (void) viewDidLoad
{
	[super viewDidLoad];
	
	NSError *error = nil;
	
	AVCaptureDevice *camera = [AVCaptureDevice defaultDeviceWithMediaType: AVMediaTypeVideo];
	AVCaptureDevice *mic = [AVCaptureDevice defaultDeviceWithMediaType: AVMediaTypeAudio];
	
	AVCaptureSession *session = [AVCaptureSession new];
	
	[session beginConfiguration];
	
	if ([camera supportsAVCaptureSessionPreset: AVCaptureSessionPreset1280x720]) {
		session.sessionPreset = AVCaptureSessionPreset1280x720;
	} else
	if ([camera supportsAVCaptureSessionPreset: AVCaptureSessionPreset640x480]) {
		session.sessionPreset = AVCaptureSessionPreset640x480;
	}
	
	AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice: camera error: &error];
	if (!input)
		[NSException raise: NSInternalInconsistencyException format: @"Cannot input from video camera device %@, %@", camera, error];
	[session addInput: input];
	
	input = [AVCaptureDeviceInput deviceInputWithDevice: mic error: &error];
	if (!input)
		[NSException raise: NSInternalInconsistencyException format: @"Cannot input from video camera device %@, %@", camera, error];
	[session addInput: input];
	
	self.fileOutput = [AVCaptureMovieFileOutput new];
	[session addOutput: self.fileOutput];
	
	[session commitConfiguration];
	
	self.previewLayer = [AVCaptureVideoPreviewLayer layerWithSession: session];
	self.previewLayer.frame = self.view.bounds;
	[self.view.layer insertSublayer: self.previewLayer atIndex: 0];
	
	[session startRunning];
}


- (void) didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
}


- (IBAction) startRecording: (id) sender
{
	NSURL *url = [NSURL fileURLWithPath: [[NSTemporaryDirectory() stringByAppendingPathComponent: [NSUUID UUID].UUIDString] stringByAppendingPathExtension: @"mov"]];
	[self.fileOutput startRecordingToOutputFileURL: url recordingDelegate: self];
}


- (IBAction) stopRecording: (id) sender
{
	[self.fileOutput stopRecording];
}


- (IBAction) close: (id) sender
{
	[self.presentingViewController dismissViewControllerAnimated: YES completion: nil];
}


- (void) captureOutput: (AVCaptureFileOutput *) captureOutput didFinishRecordingToOutputFileAtURL: (NSURL *) outputFileURL fromConnections: (NSArray *) connections error: (NSError *) error
{
	if (error) {
		UIAlertController *alert = [UIAlertController alertControllerWithTitle: error.localizedDescription message: error.localizedFailureReason preferredStyle: UIAlertControllerStyleAlert];
		[alert addAction: [UIAlertAction actionWithTitle: @"OK" style: UIAlertActionStyleDefault handler: ^(UIAlertAction *action) {
			[[NSFileManager defaultManager] removeItemAtURL: outputFileURL error: NULL];
		}]];
		[self presentViewController: alert animated: YES completion: nil];
		return;
	}
	if (CMTimeGetSeconds(captureOutput.recordedDuration) < 1.0) {
		[[NSFileManager defaultManager] removeItemAtURL: outputFileURL error: NULL];
		
		UIAlertController *alert = [UIAlertController alertControllerWithTitle: @"Video is too short" message: nil preferredStyle: UIAlertControllerStyleAlert];
		[alert addAction: [UIAlertAction actionWithTitle: @"OK" style: UIAlertActionStyleDefault handler: nil]];
		[self presentViewController: alert animated: YES completion: nil];
		return;
	}
	
	NSURL *url = [NSURL fileURLWithPath: [[NSTemporaryDirectory() stringByAppendingPathComponent: [NSUUID UUID].UUIDString] stringByAppendingPathExtension: @"mov"]];
	SDAVAssetExportSession *export = [[SDAVAssetExportSession alloc] initWithAsset: [AVAsset assetWithURL: outputFileURL]];
	export.outputFileType = AVFileTypeQuickTimeMovie;
	export.outputURL = url;
	export.videoSettings = @{
		AVVideoCodecKey: AVVideoCodecH264,
		AVVideoWidthKey: @720,
		AVVideoHeightKey: @1280,
		AVVideoCompressionPropertiesKey: @{
			AVVideoAverageBitRateKey: @2000000,
			AVVideoProfileLevelKey: AVVideoProfileLevelH264High41,
		},
	};
	export.audioSettings = @{
		AVFormatIDKey: @(kAudioFormatMPEG4AAC),
		AVNumberOfChannelsKey: @2,
		AVSampleRateKey: @44100,
		AVEncoderBitRateKey: @128000,
	};

	self.previewLayer.opacity = 0.5f;
	[self.activityIndicator startAnimating];
	self.recordButton.enabled = NO;
	
	[export exportAsynchronouslyWithCompletionHandler: ^{
		self.previewLayer.opacity = 1.0f;
		[self.activityIndicator stopAnimating];
		self.recordButton.enabled = YES;
		
		if (export.status == AVAssetExportSessionStatusCompleted) {
			[self uploadVideoFile: url duration: CMTimeGetSeconds(captureOutput.recordedDuration)];
		}
		[[NSFileManager defaultManager] removeItemAtURL: outputFileURL error: NULL];
	}];
}


- (void) uploadVideoFile: (NSURL *) outputFileURL duration: (NSTimeInterval) duration
{
	self.previewLayer.opacity = 0.5f;
	[self.activityIndicator startAnimating];
	self.recordButton.enabled = NO;
	self.progressView.hidden = NO;
	
	id movieID = nil;
	if (self.movieRepliedTo) {
		movieID = @(self.movieRepliedTo.parentIdentifier ? : self.movieRepliedTo.identifier);
	}
	duration = round(duration * 1000.0) / 1000.0;
	[self.sessionManager POST: @"/new"
				   parameters: @{
								 @"duration": @(duration),
								 @"parent": self.movieRepliedTo ? movieID : [NSNull null]
								 }
	constructingBodyWithBlock: ^(id <AFMultipartFormData> formData) {
		NSError *error = nil;
		if (![formData appendPartWithFileURL: outputFileURL
										name: @"video"
									fileName: outputFileURL.lastPathComponent
									mimeType: @"video/quicktime"
									   error: &error])
			NSLog(@"*** Can't add multipart part: %@", error);
	}
					 progress: ^(NSProgress *uploadProgress) {
						 dispatch_async(dispatch_get_main_queue(), ^{
							 self.progressView.progress = uploadProgress.fractionCompleted;
						 });
					 }
					  success: ^(NSURLSessionDataTask *task, id responseObject) {
						  NSLog(@"Success: %@", responseObject);
						  [[NSFileManager defaultManager] removeItemAtURL: outputFileURL error: NULL];
						  [self close: nil];
					  }
					  failure: ^(NSURLSessionDataTask *task, NSError *error) {
						  UIAlertController *alert = [UIAlertController alertControllerWithTitle: error.localizedDescription message: error.localizedFailureReason preferredStyle: UIAlertControllerStyleAlert];
						  [alert addAction: [UIAlertAction actionWithTitle: @"OK" style: UIAlertActionStyleDefault handler: ^(UIAlertAction *action) {
							  [[NSFileManager defaultManager] removeItemAtURL: outputFileURL error: NULL];
						  }]];
						  [self presentViewController: alert animated: YES completion: nil];
						  
						  self.previewLayer.opacity = 1.0f;
						  [self.activityIndicator stopAnimating];
						  self.recordButton.enabled = YES;
						  self.progressView.hidden = YES;
					  }];
}


#pragma mark - View Events


- (void) viewWillDisappear: (BOOL) animated
{
	[super viewWillDisappear: animated];
	[self.previewLayer.session stopRunning];
}


- (void) viewDidLayoutSubviews
{
	self.previewLayer.frame = self.view.bounds;
}


- (BOOL) prefersStatusBarHidden
{
	return YES;
}


#pragma mark - View Rotation


//- (BOOL) shouldAutorotate
//{
//	return YES;
//}


//- (NSUInteger) supportedInterfaceOrientations
//{
//	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//		return UIInterfaceOrientationMaskAll;
//	return UIInterfaceOrientationMaskAllButUpsideDown;
//}


@end
